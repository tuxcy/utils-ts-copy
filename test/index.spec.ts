import {assert, expect} from "chai";
import {CopyUtils} from "../src/CopyUtils";

describe('CopyUtils', function () {
    beforeEach(function () {
    });

    it('fastDeepCopy', function () {
        let expected1 = {
            a: {
                b: 'c'
            },
            d: [1, 2, 3],
            e: "simple string"
        };
        let map = {};
        map['circular1'] = {
            a: 0,
            link: null
        };
        map['circular2'] = {
            link: null,
            a: 'b',
            c: {
                e: [0, 1, 2]
            }
        };
        map['circular1'].link = map['circular2'];
        map['circular2'].link = map['circular1'];

        expect(CopyUtils.fastDeepCopy(expected1)).to.eql(expected1);
        expect(() => CopyUtils.fastDeepCopy(map['circular1'])).to.throw();
    });

    it('filterCopy', function () {
        let expected1 = {
            a: {
                b: 'c'
            },
            d: [1, 2, 3],
            e: "simple string"
        };
        let map = {};
        map['circular1'] = {
            a: 0,
            b: false,
            c: true,
            link: null
        };
        map['circular2'] = {
            link: null,
            a: 'b',
            c: {
                e: [0, 1, 2]
            }
        };
        map['circular1'].link = map['circular2'];
        map['circular2'].link = map['circular1'];

        expect(CopyUtils.filterCopy(expected1, ['a', 'b'])).to.eql({
            d: [1, 2, 3],
            e: "simple string"
        });
        expect(CopyUtils.filterCopy(map['circular1'], ['link'])).to.eql({
            a: 0,
            b: false,
            c: true,
        });
        expect(CopyUtils.filterCopy(map['circular1'], ['link'], true)).to.eql({
            c: true,
        });
        expect(CopyUtils.filterCopy(map['circular2'], ['link'])).to.eql({
            a: 'b',
            c: {
                e: [0, 1, 2]
            }
        });
    });

    it('partialCopy', function () {
        let object = {
            a: {
                b: {
                    c: [0, 1, 2],
                    d: "tresfr",
                    e: {
                        f: "g"
                    },
                    h: false
                }
            }
        };

        expect(CopyUtils.partialCopy(object, 1)).to.eql({
            a: {
                b: undefined
            }
        });
        expect(CopyUtils.partialCopy(object, 3)).to.eql({
            a: {
                b: {
                    c: [undefined, undefined, undefined],
                    d: "tresfr",
                    e: {
                        f: undefined
                    },
                    h: false
                }
            }
        });
        expect(CopyUtils.partialCopy(object, 5, [], true)).to.eql({
            a: {
                b: {
                    c: [0, 1, 2],
                    d: "tresfr",
                    e: {
                        f: "g"
                    }
                }
            }
        });
    });
});
