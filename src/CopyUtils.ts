class CopyUtils {
    static fastDeepCopy(obj): any {
        try {
            return JSON.parse(JSON.stringify(obj));
        } catch (error) {
            throw error;
        }
    }

    static partialCopy(o, limit = -1, excludedProps = [], clean: boolean = false) {
        function _partialCopy(o, limit, depth, excludedProps) {
            if (limit > -1 && depth > limit) return;
            if (depth > 15 && typeof o === 'object') console.warn("warning current copy has reached depth", depth, "for object", o);
            if (depth > 20 && typeof o === 'object') {
                console.warn("too many recursion may break app, object", o, "seems to cause memory leaks skipping");
                return {};
            }
            let newO, i;
            if (typeof o !== 'object') return o;
            if (!o) return o;
            if (Array.isArray(o)) {
                newO = [];
                for (i = 0; i < o.length; i += 1) {
                    newO[i] = _partialCopy(o[i], limit, depth + 1, excludedProps);
                }
                return newO;
            }
            newO = {};
            for (i in o) {
                if (o.hasOwnProperty(i) && !excludedProps.includes(i)) {
                    if (clean && o[i]) {
                        newO[i] = _partialCopy(o[i], limit, depth + 1, excludedProps);
                    } else if (!clean) {
                        newO[i] = _partialCopy(o[i], limit, depth + 1, excludedProps);
                    }
                }
            }
            return newO;
        }
        return _partialCopy(o, limit, 0, excludedProps)
    }

    static filterCopy(o, excludedProps: string[], clean: boolean = false) {
        return CopyUtils.partialCopy(o, -1, excludedProps, clean)
    }
}

export {
    CopyUtils
}
