#!/usr/bin/env bash

#GITLAB_ACCESS_TOKEN=
BADGES=`curl -s --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" https://gitlab.com/api/v4/projects/tuxcy%2Futils-ts-copy/badges`
BADGES_COUNT=`echo $BADGES | jq .[].id | wc -l`
PROJECT_ID=tuxcy%2Futils-ts-copy
PROJECT_URL=https://gitlab.com/tuxcy/utils-ts-copy

function editBadge {
  LABEL=$1
  MSG=$2
  COLOR=$3
  BADGE_INDEX=$4
  BADGE_ID=`echo "$BADGES" | jq .[${BADGE_INDEX}].id`

  echo $LABEL && echo $MSG && echo $COLOR && echo $BADGE_INDEX
  curl --request PUT --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
      --data-urlencode image_url="https://img.shields.io/static/v1?label=$LABEL&message=$MSG&color=$COLOR" \
      -d link_url="$PROJECT_URL" \
      "https://gitlab.com/api/v4/projects/$PROJECT_ID/badges/$BADGE_ID" > /dev/null
}

function razBadge {
  editBadge build pending yellow 0
  editBadge test pending yellow 1
  editBadge coverage pending yellow 2
  editBadge vulnerabilities pending yellow 3
  editBadge version pending yellow 4
}

function unitTestBadge {
  TEST_FILE_PATH=$1
  ALL_TEST=`grep "<testsuites" ${TEST_FILE_PATH} | sed -r 's/^.*tests="([^"]+).*/\1/'`
  TEST_ERROR=`grep "<testsuites" ${TEST_FILE_PATH} | sed -r 's/^.*failures="([^"]+).*/\1/'`
  COLOR=green

  TEST_DIFF=$((ALL_TEST - TEST_ERROR))

  editBadge test "$TEST_DIFF/$ALL_TEST" ${COLOR} 1
}

function coverageBadge {
  COVERAGE_FILE_PATH=$1
  RESULT=`cat "$COVERAGE_FILE_PATH" | jq .total.lines.pct`
  COLOR=green

  editBadge coverage "$RESULT%" ${COLOR} 2
}

function versionBadge {
  COLOR=green
  VERSION=`cat package.json | jq .version`

  editBadge version "${VERSION//\"}" ${COLOR} 4
}

function vulnerabilitiesBadge {
  AUDIT_FILE_PATH=$1
  COLOR=yellow
  COUNT=`(grep found "$AUDIT_FILE_PATH" | cut -d ' ' -f2)`

  editBadge vulnerabilities ${COUNT} ${COLOR} 3
}
